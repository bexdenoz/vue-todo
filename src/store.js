import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    todos: [
      // {
      //   id: Date.now(),
      //   text: 'Test 0',
      //   done: false
      // }
    ]
  },
  mutations: {
    pushTodo (state, todo) {
      state.todos.push(todo)
    },
    toggleTodo (state, updatedTodo) {
      state.todos = state.todos.map(function (todo) {
        // id'si eşleşeni değiştir
        if (todo.id === updatedTodo.id) {
          todo.done = updatedTodo.done
        }
        return todo
      })
    }
  },
  actions: {
  }
})
